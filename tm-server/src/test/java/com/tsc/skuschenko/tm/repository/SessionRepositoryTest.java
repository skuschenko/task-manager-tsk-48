package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.dto.ISessionDTORepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.repository.dto.SessionDTORepository;
import com.tsc.skuschenko.tm.service.ConnectionService;
import com.tsc.skuschenko.tm.service.PropertyService;
import com.tsc.skuschenko.tm.util.SignatureUtil;
import org.hibernate.UnresolvableObjectException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class SessionRepositoryTest {

    @NotNull
    static final IPropertyService propertyService =
            new PropertyService();

    @NotNull
    static final IConnectionService connectionService =
            new ConnectionService(propertyService);

    @NotNull
    static final EntityManager entityManager =
            connectionService.getEntityManager();

    @AfterClass
    public static void after() {
        entityManager.close();
    }

    @Test
    public void testClear() {
        @Nullable final SessionDTO session = testSessionModel();
        @NotNull final ISessionDTORepository
                sessionRepository = testRepository(session);
        entityManager.getTransaction().begin();
        sessionRepository.remove(session);
        entityManager.getTransaction().commit();
        @Nullable final SessionDTO sessionFind =
                sessionRepository.findSessionById(session);
        Assert.assertNull(sessionFind);
    }

    @Test
    public void testCreate() {
        @NotNull final SessionDTO session = testSessionModel();
        testRepository(session);
    }

    @Test
    public void testFindOneById() {
        @Nullable final SessionDTO session = testSessionModel();
        @NotNull final ISessionDTORepository
                sessionRepository = testRepository(session);
        @Nullable final SessionDTO sessionFind =
                sessionRepository.findSessionById(session);
        Assert.assertNotNull(sessionFind);
    }

    @Test(expected = UnresolvableObjectException.class)
    public void testRemoveOneById() {
        @Nullable final SessionDTO session = testSessionModel();
        @NotNull final ISessionDTORepository
                sessionRepository = testRepository(session);
        entityManager.getTransaction().begin();
        sessionRepository.removeSessionById(session);
        entityManager.getTransaction().commit();
        entityManager.refresh(session);
    }

    @NotNull
    private ISessionDTORepository testRepository(
            @NotNull final SessionDTO session
    ) {
        @NotNull final ISessionDTORepository sessionRepository =
                new SessionDTORepository(entityManager);
        @Nullable final List<SessionDTO> sessionDTOList =
                sessionRepository.findAll(session.getUserId());
        Assert.assertTrue(Optional.ofNullable(sessionDTOList).isPresent());
        entityManager.getTransaction().begin();
        sessionRepository.add(session);
        entityManager.getTransaction().commit();
        @Nullable final SessionDTO sessionById =
                sessionRepository.findSessionById(session);
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        return sessionRepository;
    }

    @NotNull
    private SessionDTO testSessionModel() {
        @Nullable final SessionDTO session = new SessionDTO();
        session.setUserId("72729b26-01dd-4314-8d8c-40fb8577c6b5");
        session.setTimestamp(System.currentTimeMillis());
        String signature = SignatureUtil.sign(session, "password", 454);
        session.setSignature(signature);
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                session.getUserId());
        Assert.assertEquals(
                signature,
                session.getSignature()
        );
        return session;
    }

}
