package com.tsc.skuschenko.tm.dto;

import com.tsc.skuschenko.tm.api.entity.IWBS;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_task")
public final class TaskDTO extends AbstractBusinessEntityDTO implements IWBS {

    @Column(name = "project_id")
    @Nullable
    private String projectId;

}
